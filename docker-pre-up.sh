#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)
echo "--project-name ${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION}"
docker-compose -f ./docker-compose.pre.run.yaml --project-name "${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION}" up -d

echo "Initialize"
echo "Sleep 1m"
sleep 1m
echo "docker exec..."

docker exec -it -u root $(docker-compose -f ./docker-compose.pre.run.yaml --project-name "${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION}" ps -q app) /bin/zsh -c 'mysql -V && echo "Sleep 1m" && sleep 1m && cd /app/pim/ && php bin/console debug:config doctrine && echo "(Re)build classes" && php bin/console pimcore:deployment:classes-rebuild && echo "Sleep 2m" && sleep 2m && echo "Warm caches" && php bin/console pimcore:cache:warming --env=prod && chmod -R 777 /app/pim/web/var && chmod -R 777 /app/pim/var' 