#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)
docker exec -it $(docker-compose -f ./docker-compose.pre.run.yaml --project-name "${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION}" ps -q apache2) bash