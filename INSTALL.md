#Installation

## Build and start

    # Build containers
    # This will delete every previously built containers and volumes. Use with care!
    $ make dev-build
    
    # Start containers
    $ make dev-up
    
    # Stop containers
    $ make dev-stop
    
## Install pimcore

    $ make dev-root
    
    
    $ rm -rf /app/pim
    $ php -d memory_limit=-1 composer.phar create-project kitt3n/pimcore-skeleton /app/pim v5.7 --keep-vcs
    
    $ /app/pim/vendor/bin/pimcore-install --admin-username sudo --admin-password='ied@i7eeGhahwah+ruHb' --no-interaction
    
    # Hardcopy assets from bundles
    $ /app/pim/bin/console assets:install
    
## Bundles

    # generate bundle interactively
    $ /app/pim/bin/console pimcore:generate:bundle

    # Add into /app/pim/app/config.yml
    
    pimcore:
    
        bundles:
            search_paths:
                - vendor/kitt3n/
                
    # Enable bundle
    $ /app/pim/bin/console pimcore:bundle:enable Kitt3nPimcoreElementsBundle  
    
    # Hardcopy assets from bundles
    $ /app/pim/bin/console assets:install