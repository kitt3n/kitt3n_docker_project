#+++++++++++++++++++++++++++++++++++++++
# Dockerfile for hon3y/hny_php_fpm:7.2
#+++++++++++++++++++++++++++++++++++++++

FROM hon3y/hny_php_fpm:7.2

# link xdebug config for osx
RUN ln -sf /opt/docker/etc/php/xdebugfordockerformac.ini /usr/local/etc/php/conf.d/xdebugfordockerformac.ini