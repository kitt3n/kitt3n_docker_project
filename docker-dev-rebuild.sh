#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)
echo "--project-name ${DEV_PREFIX}_${BUILD_PROJECT_NAME}_dev"
read -r -p "Continue? [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY])
        docker-compose -f ./docker-compose.dev.yaml --project-name "${DEV_PREFIX}_${BUILD_PROJECT_NAME}_dev" stop
        docker-compose -f ./docker-compose.dev.yaml --project-name "${DEV_PREFIX}_${BUILD_PROJECT_NAME}_dev" rm --force app apache2 phpgulp
        docker-compose -f ./docker-compose.dev.yaml --project-name "${DEV_PREFIX}_${BUILD_PROJECT_NAME}_dev" build --no-cache
        docker-compose -f ./docker-compose.dev.yaml --project-name "${DEV_PREFIX}_${BUILD_PROJECT_NAME}_dev" up -d
        ;;
    *)
        echo "Exit without building."
        ;;
esac

