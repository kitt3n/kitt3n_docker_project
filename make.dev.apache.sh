#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)
docker exec -it $(docker-compose -f ./docker-compose.dev.yaml --project-name "${DEV_PREFIX}_${BUILD_PROJECT_NAME}_dev" ps -q apache2) bash